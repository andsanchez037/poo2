/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Andres
 */
public class EmpleadoEventual extends Empleado {
    private float pagoHora;
    private float horasT;
    public EmpleadoEventual(){
        this.horasT=0.0f;
        this.pagoHora=0.0f;
}
    public EmpleadoEventual(float pagoHora, float horasT, int numEmpleado, String nombre, String puesto, String dpto){
        super (numEmpleado, nombre, puesto, dpto);
        this.pagoHora=pagoHora;
        this.horasT=horasT;
    }

    //setter and getter
    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }

    public float getHorasT() {
        return horasT;
    }

    public void setHorasT(float horasT) {
        this.horasT = horasT;
    }
    
    @Override
    public float calcularPago() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
