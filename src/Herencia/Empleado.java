/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Andres
 */
public abstract class Empleado {
   protected int numEmpleado;
   protected String nombre;
   protected String puesto;
   protected String dpto;
   
   public Empleado(){
       this.numEmpleado=0;
       this.dpto="";
       this.nombre="";
       this.puesto="";
   }
       
    public Empleado(int numEmpleado, String nombre, String puesto, String dpto){
        this.numEmpleado = numEmpleado;
        this.nombre=nombre;
        this.dpto=dpto;
        this.puesto=puesto;
    }

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getDpto() {
        return dpto;
    }

    public void setDpto(String dpto) {
        this.dpto = dpto;
    }
    public abstract float calcularPago();
    
}
