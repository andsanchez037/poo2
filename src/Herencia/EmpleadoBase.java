/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Andres
 */
public class EmpleadoBase extends Empleado implements Impuesto{
    protected float pagoDiario;
    protected float diasT;
    
    public EmpleadoBase(){
        this.diasT=0.0f;
        this.pagoDiario=0.0f;
    }

    public EmpleadoBase(float pagoDiario, float diasT, int numEmpleado, String nombre, String puesto, String dpto){
    super(numEmpleado, nombre, puesto, dpto);
    this.pagoDiario=pagoDiario;
    this.diasT=diasT;
    }

    public float getPagoDiario() {
        return pagoDiario;
    }

    public void setPagoDiario(float pagoDiario) {
        this.pagoDiario = pagoDiario;
    }

    public float getDiasT() {
        return diasT;
    }

    public void setDiasT(float diasT) {
        this.diasT = diasT;
    }
    
    
    
    @Override
    public float cacluclarImpuesto() {
        float impuesto=0;
        if(this.calcularPago()>5000) impuesto = this.cacluclarImpuesto()*0.16f;
        return impuesto;
    }

    @Override
    public float calcularPago() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

    
