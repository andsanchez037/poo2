/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author Andres
 */
public class Gasolina {
    private int id;
    private String marca; 
    private int tipo;
    private float precio;
    
    //Constructores
    public Gasolina(){
        this.marca="";
        this.id=0;
        this.tipo=0;
        this.precio=0.0f;
    }
    
    public Gasolina(int id, String marca, int tipo, float precio){
        this.marca=marca;
        this.id=id;
        this.tipo=tipo;
        this.precio=precio;
    }
    
    public Gasolina(Gasolina otro){
        this.marca=otro.marca;
        this.id=otro.id;
        this.tipo=otro.tipo;
        this.precio=otro.precio;
    }
    
    //Setter & Getter

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

   
    
    //métodos de comportamiento
    
    public String imprimirResultados(){
    String info = "";
         info=("id:" + this.id+"tipo"+this.tipo+"marca"+this.marca+"precio"+this.precio);
            return info;
        }
}